//jscs:disable requireCapitalizedComments
'use strict';

/**
 * @ngdoc directive
 * @name component-modal
 * @requires ngDialog
 * @requires jquery
 * @requires angular-templates
 * @description
 * Single/Multi step to display slides in a modal.
 */

var modalModule = angular.module('bbyModal', []);

modalModule.directive('bbyWizard', function($compile) {
  var transition;
  var forward;
  var innerHtml;
  return {
    restrict: 'A',
    templateUrl: function(element) {
      innerHtml = element.html();
      return '/components/component-modal-wrapper-default.html';
    },
    controller: function($scope, $attrs) {
      transition = $attrs.transition || 'fade-in-out';

      forward = true;

      $scope.showNavigation = $attrs.navigation || false;

      $scope.currentSection = 1;

      $scope.finishButton = false;

      if (typeof $attrs.onFinish !== 'undefined') {
        $scope.finishButton = true;
      }
    },
    link: function(scope, element, attrs) {
      var customClass = attrs.customClass || 'medium';

      var wrapper = jQuery(element).find('.component-modal-wrapper');

      wrapper.addClass(customClass);

      var children = jQuery(innerHtml);

      wrapper.append(children);

      var sections = jQuery(element).find('div[bby-wizard-section]');

      scope.totalSections = sections.length;

      sections.each(function(index) {
        jQuery(this).attr('ng-class', 'getTransitionFX()');
        jQuery(this).attr('ng-show', 'getCurrentStep() ==' + (index + 1));
      });

      $compile(children)(scope);

      scope.getTransitionFX = function() {
        var trans;

        switch (transition) {
          case 'slide':
            if (forward) {
              trans = 'slide-left';
            } else {
              trans = 'slide-right';
            }
            break;
          default:
            trans = transition;
            break;
        }

        return trans;
      };

      scope.prevSection = function(shouldUpdateCurrent) {
        shouldUpdateCurrent = typeof shouldUpdateCurrent !== 'undefined' ? shouldUpdateCurrent : true;

        jQuery(element).find('.slide-left')
          .removeClass('slide-left')
          .addClass('slide-right');

        forward = false;

        if (scope.currentSection > 0 && shouldUpdateCurrent) {
          scope.currentSection--;
        }
      };

      scope.getCurrentStep = function() {
        return scope.currentSection;
      };

      scope.nextSection = function(shouldUpdateCurrent) {
        shouldUpdateCurrent = typeof shouldUpdateCurrent !== 'undefined' ? shouldUpdateCurrent : true;

        jQuery(element).find('.slide-right')
          .removeClass('slide-right')
          .addClass('slide-left');

        forward = true;

        if (scope.currentSection < scope.totalSections && shouldUpdateCurrent) {
          scope.currentSection++;
        }
      };

      scope.goToSection = function(sectionDestination, slideFrom) {
        slideFrom = typeof slideFrom !== 'undefined' ? slideFrom : 'right';

        if (slideFrom === 'left') {
          scope.prevSection(false);
        } else {
          scope.nextSection(false);
        }

        scope.currentSection = sectionDestination;
      };

      scope.onFinish = function() {
        if (scope.finishButton) {
          scope.$eval(attrs.onFinish);
        }
      };
    }
  };
});

modalModule.directive('bbyWizardSection', function($compile, $templateCache) {
  var defaultHeaderTemplate = '/components/component-modal-header-default.html';
  var defaultBodyTemplate = '/components/component-modal-body-default.html';
  var defaultFooterTemplate = '/components/component-modal-footer-default.html';
  var defaultNavigationTemplate = '/components/component-modal-navigation-default.html';

  return {
    restrict: 'A',
    scope: false,
    link: function(scope, element, attrs) {
      element.addClass('ui-modal-dialog');

      // Getting custom template or default one
      var headerTpl = $templateCache.get(attrs.headerTemplate) || $templateCache.get(defaultHeaderTemplate);

      // Creating an isolated scoped to prevent scope conflicts
      var headerScope = scope.$new();

      headerScope.componentModalTitle = attrs.title;
      headerScope.enableBackButton = attrs.enableBackButton || false;
      headerScope.backTo = attrs.backTo || false;

      // Creating angular element and compiling it
      var headerElem = angular.element(headerTpl);
      $compile(headerElem)(headerScope);

      // Getting custom template or default one
      var bodyTpl = $templateCache.get(attrs.bodyTemplate) || $templateCache.get(defaultBodyTemplate);

      // Getting section content either from a template or html
      var bodySection = $templateCache.get(attrs.template) || element.html();

      // Create angular element
      var contentElem = angular.element(bodyTpl);

      // Append bodySection to parent Body Element
      contentElem.append(bodySection);

      // Compiling with global scope
      $compile(contentElem)(scope);

      // Getting custom template or default one
      var footerTpl = $templateCache.get(attrs.footerTemplate) || $templateCache.get(defaultFooterTemplate);

      // Creat isolated scope for footer
      var footerScope = scope.$new(true);
      footerScope.componentModalEnableNavigation = scope.$eval(attrs.enableNavigation || scope.showNavigation);

      // Create footer angular element
      var footerElem = angular.element(footerTpl);

      // Compiling with global scope
      $compile(footerElem)(footerScope);

      // Getting navigation content either from a template or default one
      var navigationTpl = $templateCache.get(attrs.navigationTemplate) || $templateCache.get(defaultNavigationTemplate);

      // Create navigation angular element
      var navigationElem = angular.element(navigationTpl);

      // Compiler navigation with global scope
      $compile(navigationElem)(scope);

      // Append bodySection to parent Body Element
      jQuery(footerElem).find('div.component-modal-navigation').append(navigationElem);

      element.empty();
      element.append(headerElem);
      element.append(contentElem);
      element.append(footerElem);

      if (typeof attrs.onEnter !== 'undefined') {
        scope.$eval(attrs.onEnter);
      }
    }
  };
});
