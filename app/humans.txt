# humanstxt.org/
# The humans responsible & technology colophon

# TEAM

  Thales Pinheiro -- Software Architecture and Engineer
  Your name -- Your title

# THANKS

  Name -- Title

# TECHNOLOGY COLOPHON

  HTML5, CSS3, SCSS, Bootstrap3, Bower, Gulp, JS, Angular
