'use strict';

/**
 * @author Thales Pinheiro
 * @since 03/08/2015
 * @access private
 * @copyright Best Buy Canada
 * Protractor E2E configuration file
 */

exports.config = {
  allScriptsTimeout: 30000,
  // TODO: BASEURL address - it can broke on the Integration Test
  baseUrl: 'http://127.0.0.1:3000/',
  capabilities: {
    'browserName': 'phantomjs'
  },
  directConnect: false,
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    isVerbose: true,
    includeStackTrace: false
  },
  specs: ['spec/*.js']
};
