// jscs:disable requireCapitalizedComments
// jscs:disable maximumLineLength

'use strict';

/**
 * @author Thales Pinheiro
 * @contributors
 * - Mike Buckley
 * - Fernando Pintado
 * @since 03/08/2015
 * @access private
 * @copyright Best Buy Canada
 * Protractor E2E test file
 */

describe('Demo Page', function() {
  // navigate to the main page
  browser.get('/');

  it('should display a button', function() {
    var button = element(by.css('.btn-demo'));
    expect(button.isPresent()).toBe(true);
    button.click();
  });

  it('should open the modal', function() {
    var modal = element(by.css('.ngdialog'));
    expect(modal.isPresent()).toBe(true);
  });

  it('should load dynamic section', function() {
    var title = element(by.cssContainingText('.ui-modal-dialog .modal-title', 'Dynamic'));
    expect(title.isPresent()).toBe(true);
  });

  it('should hide back button and show next button', function() {
    var nextButton = element(by.cssContainingText('.navigation a', 'Next'));
    var backButton = element(by.cssContainingText('.navigation a', 'Back'));
    expect(nextButton.isPresent()).toBe(true);
    expect(backButton.isPresent()).toBe(false);
    nextButton.click();
    browser.driver.sleep(400);
  });

  it('should load static section', function() {
    var title = element(by.cssContainingText('.ui-modal-dialog .modal-title', 'Static'));
    expect(title.isPresent()).toBe(true);
  });

  it('should display back and finish button', function() {
    var finishButton = element(by.cssContainingText('.navigation a', 'Finish'));
    var backButton = element(by.cssContainingText('.navigation a', 'Back'));
    expect(finishButton.isPresent()).toBe(true);
    expect(backButton.isPresent()).toBe(true);
  });
});
