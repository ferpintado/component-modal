'use strict';

/**
 * @author Mike Buckley
 * @since 03/08/2015
 * @contributors
 * - Fernando Pintado
 * @access private
 * @copyright Best Buy Canada
 * Unit test of the modal directive
 */

describe('Directive: modal', function() {
  var scope;
  var compile;
  var dscope;
  var element;

  // Load the directive's module
  beforeEach(module('modal'));

  beforeEach(module('modal-templates'));

  beforeEach(inject(function($rootScope, $compile) {
    scope = $rootScope.$new();
    compile = $compile;
  }));

  beforeEach(function() {
    var html = '<div bby-wizard class="slide-container" ' +
      'steps="2" navigation="true" >' +
      '<div bby-wizard-section><p>Section 1</p></div>' +
      '<div bby-wizard-section><p>Section 2</p></div> ' +
   ' </div>';
    element = angular.element(html);

    // compiles to angular
    compile(element)(scope);

    // gets directive scope
    dscope = element.scope();
    dscope.$digest();


  });

  it('should initialize directive', function() {
    expect(dscope.getCurrentStep()).toBe(1);
    expect(dscope.totalSections).toBe(2);
  });

  it('should go to next section', function() {
    dscope.nextSection();
    expect(dscope.getCurrentStep()).toBe(2);
  });

  it('should go to previous section', function() {
    dscope.nextSection();
    dscope.prevSection();
    expect(dscope.getCurrentStep()).toBe(1);
  });

});
