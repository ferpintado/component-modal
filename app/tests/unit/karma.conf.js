'use strict';

/**
 * @author Thales Pinheiro
 * @since 03/08/2015
 * @access private
 * @copyright Best Buy Canada
 * @see http://karma-runner.github.io/0.12/config/configuration-file.html
 * Karma test runner configuration file
 */

module.exports = function(config) {
  config.set({
    // Enable or disable watching file and executing tests whenever any file changes
    autoWatch: true,

    // Base path, that will be used to resolve files and exclude
    basePath: '../../',

    // Testing framework to use (jasmine/mocha/qunit/...)
    frameworks: ['jasmine'],

    // List of files / patterns to load in the browser
    files: [
      // Bower:js
      'vendor/angular/angular.js',
      'vendor/angular-mocks/angular-mocks.js',
      'vendor/angular-translate/angular-translate.js',
      'vendor/jquery/dist/jquery.js',
      '../dist/scripts/modal-templates.js',

      // App
      'scripts/**/*.js',
      'tests/unit/mock/**/*.js',
      'tests/unit/spec/**/*.js'
    ],

    // list of files / patterns to exclude
    exclude: [
    ],

    // web server port
    port: 9000,

    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    browsers: [
      'PhantomJS'
    ],

    // Which plugins to enable
    plugins: [
      'karma-phantomjs-launcher',
      'karma-jasmine',
      'karma-coverage',
      'karma-junit-reporter',
      'karma-mocha-reporter'
    ],

    /*
     * Continuous Integration mode
     * if true, it capture browsers, run tests and exit
     */
    singleRun: false,
    colors: true,

    /*
     * Level of logging
     * possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
     */
    logLevel: config.LOG_INFO,

    /* Source files, that you wanna generate coverage for
     * do not include tests or libraries
     * (these files will be instrumented by Istanbul)
     */
    preprocessors: {
      'scripts/**/*.js': ['coverage']
    },

    // Coverage reporter generates the coverage
    reporters: ['mocha', 'junit', 'coverage'],

    junitReporter: {
      outputFile: '../coverage/unit/TESTS-xunit.xml'
    },

    // Optionally, configure the reporter
    coverageReporter: {
      // Specify a common output directory
      dir: '../coverage/',
      // Reporters not supporting the `file` property
      reporters: [{
        type: 'html',
        subdir: 'report-html'
      }, {
        type: 'lcov',
        subdir: 'report-lcov'
      }]
    }
  });
};
