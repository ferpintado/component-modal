# Modal Component

Directive to load dynamic and static content separated by sections (optional).

This is a helper to be used along ngDialog or by itself. 

## Installation

You need to have installed all NGA development requirements (node, gulp, bower....)

```sh
$ bower install component-modal
```

## Dependencies
- ngDialog
- ngAnimate
- jQuery
- angular-templates

### bby-wizard

#### Optional attributes
`transition` *(fade-in-out / slide)*   **Default:** `fade-in-out`  
Type of effect transition  
`navigation` *(true / false)* **Default:** `false`   
Display / hide navigation  
`custom-class` **Default:** `medium`  
Add custom class name(s) to the wrapper.  
`on-finish`  
Will add a "Finish" button on the last section and will $eval the expression when clicked.

### bby-wizard-section
#### Required attributes 
`title`  
Header title

#### Optional attributes  
`template`  
Angular template id   
`header-template`    **Default** `component-modal-header-default.html`  
Will use this template as header template  
`enable-back-button` *(true / false)* **Default:** `true`  
Will append a back button on the upper left corner  
`back-to`  *(int)*  
Section number destination when back button (see above) is clicked.  
`body-template`    **Default** `component-modal-body-default.html`  
Will use this template as body template    
`template`  *(angular template)*  
Will use this template as content (will be appended to body-template)  
`footer-template`    **Default** `component-footer-body-default.html`  
Will use this template as footer template    
`footer-template`    **Default** `component-footer-body-default.html`  
Will use this template as footer template   
`enable-navigation` *(true/false)*   
Will overwrite bby-wizard navigation settings  
`navigation-template`    **Default** `component-footer-navigation-default.html`  
Will use this template as navigation template (will be appended to footer-template to ***div.component-modal-navigation*** element)  
`on-enter`  
Function triggers when section is rendered

## Usage
#### HTML
````html
<a href="#" data-ng-click="openModal()" class="btn btn-primary">Open Modal</a>
```
#### Single Modal Template
```html
<div bby-wizard>
    <div bby-wizard-section title="Single Modal - Inline HTML">
        <h1>Static</h1>
        <p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper.</p>
    </div>
</div>
```

#### Multistep Modal Template
```html
<div bby-wizard class="slide-container" transition="slide" navigation="true" on-finish="finishAction()">
  <div bby-wizard-section title="Multistep - Inline HTML">
    <h1>Static</h1>
    <p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrumorci, sagittis tempus lacus enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p>
  </div>
  <div bby-wizard-section template="dynamic" on-enter="getProducts()" title="Dynamic Content"></div>
  <div bby-wizard-section title="Multistep - No navigation" enable-navigation="false">
    <h1>I have no navigation</h1>
    <p>Theres no way to go back, muahahahaha >:)</p>
  </div>
</div>
```

#### Controller
```js
    var app = angular.module('exampleApp', ['modal', 'ngAnimate', 'ngDialog']);
    app.factory('ProductsFactory', function() {
      var url = 'http://www.bestbuy.ca/api/v2/json/search';
      return {
        all: function() {
          return $.ajax({
            url: url,
            type: 'get',
            jsonpCallback: 'products',
            dataType: 'jsonp',
            cache: true
          });
        }
      };
    });
    app.controller('MainController', ['$scope', 'ProductsFactory', 'ngDialog', function($scope, ProductsFactory, ngDialog) {
      $scope.products = [];
      
      $scope.openMultiStepModal = function() {
        ngDialog.open({
          template: 'multistep-modal',
          scope: $scope,
          showClose: false
        });
      };
      
      $scope.getProducts = function() {
        ProductsFactory.all().done(function(data) {
          $scope.products = data.products;
          $scope.$digest();
        });
      };
      
      $scope.finishAction = function() {
        alert('This is the end');
      };
    }]);
```

#### Available functions

`$scope.getCurrentStep()`  
Return current step  
`$scope.nextSection()`  
Go to the next consecutive section  
`$scope.prevSection()`  
Go to the previous consecutive section  
`$scope.goToSection(section,direction)`  *(int , left/right)*  
Go to another defined section.  


#### Custom navigation
You are able to create custom navigation.
##### Example
**custom-navigation.html**
```html
<a ng-click="goToSection(1,'left')" class="btn btn-primary">Let's go where everything started!</a>
```
**modal.html**
```html
<div bby-wizard navigation="false">
    <div bby-wizard-section enable-navigation="true" navigation-template="custom-navigation.html" title="Custom navigation">
        <p>I got my own special custom navigation</p>
    </div>
</div>
```

